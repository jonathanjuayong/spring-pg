package com.jonathanjuayong.springpg.requests

data class CityRequest(
    val name: String,
    val code: String,
    val countryId: Long
)