package com.jonathanjuayong.springpg

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class SpringPgApplication

fun main(args: Array<String>) {
    runApplication<SpringPgApplication>(*args)
}
