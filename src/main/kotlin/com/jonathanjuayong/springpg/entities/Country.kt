package com.jonathanjuayong.springpg.entities

import com.fasterxml.jackson.annotation.JsonIgnore
import javax.persistence.*

@Entity
@Table(name = "countries")
class Country(
    @Column(name = "country_name", columnDefinition = "varchar(50) not null")
    var countryName: String = "",

    @JsonIgnore
    @OneToMany
    @JoinColumn(name = "country_id")
    var cities: Set<City> = setOf(),

    @Id
    @Column(name = "country_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var countryId: Long = 0
)