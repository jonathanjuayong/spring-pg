package com.jonathanjuayong.springpg.entities

import javax.persistence.*

@Entity
@Table(name = "cities")
class City(
    @Column(name = "city_name")
    var cityName: String = "",

    @Column(name = "city_code", columnDefinition = "varchar(3) not null default 'NA'")
    var cityCode: String = "",

    @ManyToOne
    @JoinColumn(name = "country_id")
    var country: Country,

    @Id
    @Column(name = "city_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long = 0,
)