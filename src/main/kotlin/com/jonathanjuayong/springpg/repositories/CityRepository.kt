package com.jonathanjuayong.springpg.repositories

import com.jonathanjuayong.springpg.entities.City
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CityRepository : JpaRepository<City, Long>