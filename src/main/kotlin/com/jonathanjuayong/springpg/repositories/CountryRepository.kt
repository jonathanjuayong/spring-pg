package com.jonathanjuayong.springpg.repositories

import com.jonathanjuayong.springpg.entities.Country
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository

@Repository
interface CountryRepository: JpaRepository<Country, Long>