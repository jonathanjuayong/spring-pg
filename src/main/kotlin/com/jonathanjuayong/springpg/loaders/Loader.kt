package com.jonathanjuayong.springpg.loaders

import com.jonathanjuayong.springpg.entities.City
import com.jonathanjuayong.springpg.entities.Country
import com.jonathanjuayong.springpg.repositories.CityRepository
import com.jonathanjuayong.springpg.repositories.CountryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Component
import javax.annotation.PostConstruct

@Component
class Loader(
    @Autowired val cityRepository: CityRepository,
    @Autowired val countryRepository: CountryRepository,
) {

    @PostConstruct
    fun loadData() {
        val US = Country("US")
        val taiwan = Country("Taiwan")
        val ph = Country("Philippines")

        val manila = City("Manila", "MNL", ph)
        val kaohsiung = City("Kaohsiung", "KHS", taiwan)
        val newYork = City("New York", "NY", US)

        countryRepository.saveAll(listOf(US, taiwan, ph))
        cityRepository.saveAll(listOf(manila, kaohsiung, newYork))
    }
}
