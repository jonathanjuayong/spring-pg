package com.jonathanjuayong.springpg.services

import com.jonathanjuayong.springpg.entities.Country
import com.jonathanjuayong.springpg.exceptions.ResourceNotFoundException
import com.jonathanjuayong.springpg.repositories.CountryRepository
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class CountryService(@Autowired val countryRepository: CountryRepository) {
    fun getCountries() =
        countryRepository.findAll()

    fun getCountry(id: Long) =
        countryRepository
            .findById(id)
            .orElseThrow { ResourceNotFoundException("Country with id $id is not found") }

    fun postCountry(country: Country) =
        countryRepository.save(country)

    fun putCountry(id: Long, country: Country) =
        if (countryRepository.existsById(id))
            ResponseEntity(countryRepository.save(country), HttpStatus.OK)
        else
            ResponseEntity(countryRepository.save(country), HttpStatus.CREATED)

    fun deleteCountry(id: Long) =
        countryRepository.deleteById(id)
}