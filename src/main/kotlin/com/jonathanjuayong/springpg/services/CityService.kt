package com.jonathanjuayong.springpg.services

import com.jonathanjuayong.springpg.entities.City
import com.jonathanjuayong.springpg.exceptions.ResourceNotFoundException
import com.jonathanjuayong.springpg.repositories.CityRepository
import com.jonathanjuayong.springpg.repositories.CountryRepository
import com.jonathanjuayong.springpg.requests.CityRequest
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service

@Service
class CityService(
    @Autowired val cityRepository: CityRepository,
    @Autowired val countryRepository: CountryRepository,
) {
    fun getCities() =
        cityRepository.findAll()

    fun getCity(id: Long) =
        cityRepository
            .findById(id)
            .orElseThrow { ResourceNotFoundException("City with id $id is not found") }

    fun postCity(cityRequest: CityRequest) {
        val (name, code, countryId) = cityRequest
        val country = countryRepository
            .findById(countryId)
            .orElseThrow { ResourceNotFoundException("Country with id $countryId does not exist") }
        cityRepository.save(City(name, code, country))
    }

    fun putCity(id: Long, city: City) =
        if (cityRepository.existsById(id))
            ResponseEntity(cityRepository.save(city), HttpStatus.OK)
        else
            ResponseEntity(cityRepository.save(city), HttpStatus.CREATED)

    fun deleteCity(id: Long) =
        cityRepository.deleteById(id)
}