package com.jonathanjuayong.springpg.controllers

import com.jonathanjuayong.springpg.entities.City
import com.jonathanjuayong.springpg.entities.Country
import com.jonathanjuayong.springpg.exceptions.ResourceNotFoundException
import com.jonathanjuayong.springpg.requests.CityRequest
import com.jonathanjuayong.springpg.services.CityService
import com.jonathanjuayong.springpg.services.CountryService
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.bind.annotation.*
import kotlin.jvm.Throws

@RestController
@RequestMapping("/api/v1/cities")
class HomeController(@Autowired val cityService: CityService) {

    @GetMapping
    fun getCities() =
        cityService.getCities()

    @GetMapping("/{id}")
    @Throws(ResourceNotFoundException::class)
    fun getCity(@PathVariable id: Long) =
        cityService.getCity(id)

    @PostMapping
    fun postCity(@RequestBody cityRequest: CityRequest) =
        cityService.postCity(cityRequest)

    @PutMapping("/{id}")
    fun putCity(@PathVariable id: Long, @RequestBody city: City) =
        cityService.putCity(id, city)

    @DeleteMapping("/{id}")
    fun deleteCity(@PathVariable id: Long) =
        cityService.deleteCity(id)
}

@RestController
@RequestMapping("/api/v1/countries")
class CountryController (@Autowired val countryService: CountryService) {

    @GetMapping
    fun getCountries() =
        countryService.getCountries()

    @GetMapping("/{id}")
    @Throws(ResourceNotFoundException::class)
    fun getCountry(@PathVariable id: Long) =
        countryService.getCountry(id)

    @PostMapping
    fun postCountry(@RequestBody country: Country) =
        countryService.postCountry(country)

    @PutMapping("/{id}")
    fun putCountry(@PathVariable id: Long, @RequestBody country: Country) =
        countryService.putCountry(id, country)

    @DeleteMapping("/{id}")
    fun deleteMapping(@PathVariable id: Long) =
        countryService.deleteCountry(id)

}