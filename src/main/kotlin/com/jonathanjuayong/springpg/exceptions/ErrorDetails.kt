package com.jonathanjuayong.springpg.exceptions

import java.util.*

class ErrorDetails(
    val timestamp: Date,
    val message: String,
    val details: String
)